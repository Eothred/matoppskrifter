Oppskriftene lagret her er i formatet JSON, og følger strukturen for kokebøker definert av [https://schema.org/Recipe](schema.org)

Disse kan f.eks. brukes i Nextcloud sin kokebok-applikasjon.

Ellers har jeg noen i markdown, som du finner på wiki her: https://gitlab.com/Eothred/matoppskrifter/wikis/Matoppskrifter
